<?php

namespace PhotoFinder;

use Crew\Unsplash\PageResult;
use Crew\Unsplash\Search;

class Find
{
    use PhotoLibraries;

    public function __construct()
    {
        $this->initLibraries();
    }

    /**
     * Call photo client to do a string search
     *
     * @param $query
     * @return array
     * @throws \Exception
     */
    public static function photos($query)
    {
        try {

            /** @var PageResult $request */
            $request = Search::photos($query, 1, 10);

        } catch (\Exception $e) {

            throw new \Exception('getPhotos API Error:', [$e->getMessage(), $e->getFile(), $e->getLine()]);

        }

        return $request->getResults();
    }
}
