<?php

namespace PhotoFinder;

use Crew\Unsplash\HttpClient;

trait PhotoLibraries
{
    public function initLibraries()
    {
        $this->initUnsplash();
    }

    private function initUnsplash()
    {
        HttpClient::init([
            'applicationId'	=> '2b458597c339cb7fd0320008f15a258f760e039a7390bed68583eadb1a819b83',
            'secret'		=> '003e681b76fc5ba1e34a723d338547101aa99a1ee460d9c8395a65147a737e21',
            'callbackUrl'	=> '',
            'utmSource' => 'Fake Place'
        ]);
    }
}